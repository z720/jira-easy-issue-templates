package ru.andreymarkelov.atlas.plugins.templator;

public enum ExportType {
    PDF, DOCX;
}
