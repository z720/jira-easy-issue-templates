package ru.andreymarkelov.atlas.plugins.templator;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraWebUtils;

import java.util.HashMap;
import java.util.Map;

public class LoaderPanel extends AbstractJiraContextProvider {
    private final TemplateStoreDataService dataService;

    public LoaderPanel(TemplateStoreDataService dataService) {
        this.dataService = dataService;
    }

    @Override
    public Map<String, Object> getContextMap(ApplicationUser user, JiraHelper helper) {
        Boolean inStatus = Boolean.FALSE;
        Issue issue = (Issue) helper.getContextParams().get("issue");
        for (TemplateStoreData data : dataService.getTemplateStoreDataList()) {
            if (data != null
                   && data.getStatuses().contains(issue.getStatus().getId())
                   && data.getProjectKey().equals(issue.getProjectObject().getKey())
                   && data.getIssueTypeKey().equals(issue.getIssueType().getId())) {
                inStatus = Boolean.TRUE;
            }
        }

        Map<String, Object> contextMap = new HashMap<String, Object>();
        contextMap.put("basePath", Utils.getBaseUrl(JiraWebUtils.getHttpRequest()));
        contextMap.put("issue", issue);
        contextMap.put("inStatus", inStatus);
        return contextMap;
    }
}
