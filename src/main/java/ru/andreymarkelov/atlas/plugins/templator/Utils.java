package ru.andreymarkelov.atlas.plugins.templator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

public class Utils {
    public static String getBaseUrl(HttpServletRequest req) {
        return req.getScheme().concat("://").concat(req.getServerName()).concat(":").concat(Integer.toString(req.getServerPort())).concat(req.getContextPath());
    }

    public static String getStringFromInputStream(FileItem fileItem) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(fileItem.getInputStream(), "UTF-8"));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public static File getTemplateDir(Long templateId) {
        File jiraHome = ComponentAccessor.getComponent(JiraHome.class).getHome();
        File jiraHomeTempl = new File(jiraHome, Consts.TEMPLATE_DIRECTORY);
        if (!jiraHomeTempl.exists()) jiraHomeTempl.mkdirs();
        File templateHome = new File(jiraHomeTempl, templateId.toString());
        if (!templateHome.exists()) templateHome.mkdirs();
        return templateHome;
    }

    public static File getTemplateFile(TemplateStoreData data) {
        if (data == null) {
            return null;
        }
        File templateDir = getTemplateDir(data.getId());
        return new File(templateDir, data.getTemplateName());
    }

    public static String listLongsToStr(Set<Long> list) {
        StringBuilder sb = new StringBuilder();
        if (list != null && !list.isEmpty()) {
            for (Number l : list) {
                sb.append(l.toString()).append("&");
            }
        }
        return sb.toString();
    }

    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                return map.get(k1).compareTo(map.get(k2));
            }
        };
        Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }

    public static Set<Long> strToListLongs(String str) {
        Set<Long> list = new HashSet<Long>();
        if (str == null || str.isEmpty()) {
            return list;
        }

        StringTokenizer st = new StringTokenizer(str, "&");
        while (st.hasMoreTokens()) {
            try {
                list.add(Long.valueOf(st.nextToken()));
            } catch (NumberFormatException nex) {
            }
        }
        return list;
    }

    private Utils() {}
}
